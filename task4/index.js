function IsPowOfTwo(value) {
    return value && !(value & (value - 1));
}

// task 2

function findChrome() {
    let str = 'Ми знаємо, що монохромний колір – це градації сірого'
    let arr = str.split(' ');
    let result = [];
    for (let i = 0; i < arr.length; i++) {
        if (arr[i].includes('хром')) {
            result.push(arr[i]);
        }
    }
    return result;
}

// task 3

function StrPad(input, fullLen, fillStr, fillType) {
    let diff = fullLen - input.length;
    let result = input;
    if (fillType === 'FILL_RIGHT' || fillType === undefined) {
        while (result.length < fullLen) {
            result += fillStr;
            result = result.slice(0, fullLen);
        }
    } else if (fillType === 'FILL_LEFT') {
        while (result.length < fullLen) {
            result = fillStr + result;
            result = result.slice(0, diff) + input
        }
    } else if (fillType === 'FILL_BOTH') {
        if (diff % 2 === 0) {
            while (result.length < fullLen) {
                result = fillStr.slice(0, (diff/2)) + result + fillStr.slice(0, (diff/2));
            }
        } 
        else {
            while (result.length < fullLen) {
                result = fillStr.slice(0, (diff/2)) + result + fillStr.slice(0, (diff/2) + 1);
            }
        }
    }
    return result;
}

// task 4

function regexDate(date) {
    let regex = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\-]\d{4}$/;
    return regex.test(date);
}

// task 5

function RegStr (name) {
    return name.replace(/(\w+), (\w+)/gi, '$2 $1');
}