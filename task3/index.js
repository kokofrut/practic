function isEven(x) {
    if ((x & 1) == 0) {
      return true;
    } 
    else {
      return false;
    }
  }

// task2

function testString(value) {
    const temp = []
    const start = ['(','[']
    const end = [')',']']
    for (let letter of value) { 
        if(start.includes(letter)){
            temp.push(letter)
        }else if(end.includes(letter)){
            const match = start[end.indexOf(letter)]
            if(temp[temp.length - 1] === match){ 
                temp.splice(-1,1)
            }else{
                temp.push(letter)
                break
            }
        }
    }
    return (temp.length === 0)
}

// testString('isu([syvstc]ts(crs))cs') // true

// task3

class circleArray {
    constructor(options) {
        this.arr = []
    }
    add (value) {
        return this.arr.push(value)
    }
    get (i) {
        let n = this.arr.length
        return this.arr[(i % n + n) % n]
    }
}

let circledArray = new circleArray()
circledArray.add("Київ");
circledArray.add("Харків");
circledArray.add("Херсон");
circledArray.get(4)
circledArray.get(-1)

// task4

class Rectangle {
    constructor(width, height) {
        this.width = width;
        this.height = height;
    }
    draw() {
        let i = document.createElement('div');
        i.className = 'rectangle';
        i.style.width = this.width + 'px';
        i.style.height = this.height + 'px';
        i.style.backgroundColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
        document.body.appendChild(i);
    }
}

class Triangle {
    constructor(width, height) {
        this.width = width;
        this.height = height;
    }
    draw() {
        let i = document.createElement('div');
        i.className = 'triangle';
        i.style.width = '0px';
        i.style.height = '0px';
        i.style.border = this.width + 'px solid transparent';
        i.style.borderTop = 0;
        i.style.borderBottom = this.height + 'px solid #' + Math.floor(Math.random() * 16777215).toString(16);
        document.body.appendChild(i);
    }
}

let rect = new Rectangle(200, 100);
rect.draw();
let triangle = new Triangle(100, 100);
triangle.draw();

// task5

function substrCount(input, needle, offset, length) {
    let count = 0;
    let range = offset + length
    for (let i = offset; i < range; i++) {
        if (input.substring(i, i + needle.length) === needle) {
            count++;
        }
    }
    return count;
}

substrCount('abcdebc', 'bc', 3, 4) // 1