let menu = document.getElementById('pages');

let menuItems = menu.getElementsByTagName('li');



for (let i = 0; i < menuItems.length; i++) {
    menuItems[i].onclick = function () {
        let active = document.getElementsByClassName('active');
        active[0] ? (active[0].classList.remove('active')) : '';
        console.log(active[0].classList)
        console.log();
        menuItems[i].classList.add('active');
        menuItems[i].style.backgroundColor = 'black';
        menuItems[i].firstChild.style.color = 'white';
    }
}
console.log(menuItems);

function checkForm() {
    var form = document.getElementById('form-vertical');
    console.log(form)
    var flag = true;
    for (var i = 0; i < form.length; i++) {
        if (form[i].value == '') {
            form[i].style.borderColor = 'red';
            flag = false;
        }
    }
    if (flag) {
        form.submit();
    } else {
        alert('Все поля должны быть заполнены!');
    }
}

let submit = document.getElementById('submit');
submit.onclick = checkForm;

// make online store page
let PRODUCTS = [
    {
        id: 1,
        name: 'Станок для разделки кабеля Popcorn',
        price: '2000',
        weight: '12',
        engine: '120',
        diametr: '1,1-24',
        img: 'src/stanok1.jpg'
    },
    {
        id: 2,
        name: 'Станок для разделки кабеля Lilos',
        price: '3000',
        weight: '13',
        engine: '140',
        diametr: '1,2-20',
        img: 'src/stanok2.jpg'
    },
    {
        id: 3,
        name: 'Станок для разделки кабеля Popdddw',
        price: '4000',
        weight: '11',
        engine: '180',
        diametr: '1,5-30',
        img: 'src/stanok3.jpg'
    },
    {
        id: 4,
        name: 'Станок для разделки кабеля Popdddw',
        price: '5000',
        weight: '12',
        engine: '200',
        diametr: '1,5-30',
        img: 'src/stanok1.jpg'
    },
    {
        id: 5,
        name: 'Станок для разделки кабеля Wawa',
        price: '6000',
        weight: '13',
        engine: '220',
        diametr: '1,5-30',
        img: 'src/stanok2.jpg'
    },
    {
        id: 6,
        name: 'Станок для разделки кабеля lol',
        price: '7000',
        weight: '11',
        engine: '240',
        diametr: '1,5-30',
        img: 'src/stanok3.jpg'
    },
    {
        id: 7,
        name: 'Станок для разделки кабеля Popdddw',
        price: '9000',
        weight: '11',
        engine: '230',
        diametr: '1,5-30',
        img: 'src/stanok1.jpg'
    },
    {
        id: 8,
        name: 'Станок для разделки кабеля Wawa',
        price: '11000',
        weight: '13',
        engine: '260',
        diametr: '1,5-30',
        img: 'src/stanok2.jpg'
    },
    {
        id: 9,
        name: 'Станок для разделки кабеля lol',
        price: '12000',
        weight: '15',
        engine: '300',
        diametr: '1,5-30',
        img: 'src/stanok3.jpg'
    }


];
let productsContainer = document.getElementsByClassName('show-container');

let showcont
let n = 0
var showProductsRows = function() {
    const container = document.getElementById('flex-items-right');
    for (let i = 0; i < 3; i++) {
        let productsList = '';
        showcont = document.createElement("div")
        showcont.className = "show-container"
        container.appendChild(showcont)
        for (let j = n; j < n + 3; j++) {
            productsList += 
            `<div class="show-box">
                <img src = '${PRODUCTS[j].img}'></img>
                <p class="show-box-title">${PRODUCTS[j].name}</p>
                <table class="prefs">
                    <tr class="prefs-row">
                        <td class="prefs-cell">Вага</td>
                        <td class="prefs-cell">${PRODUCTS[j].weight}кг</td>
                    </tr>
                    <tr class="prefs-row">
                        <td class="prefs-cell">Двигун</td>
                        <td class="prefs-cell">${PRODUCTS[j].engine} Вт</td>
                    </tr>
                    <tr class="prefs-row">
                        <td class="prefs-cell">Діам. обр.</td>
                        <td class="prefs-cell">${PRODUCTS[j].diametr} мм</td>
                    </tr>
                </table>
                <p class="show-box-price"> <span>${PRODUCTS[j].price}</span> грн</p>
                <form action="product.html" target="_blank">
                    <input type="hidden" name="id" value="1"></input>
                    <input type="submit" value="Купити" class="show-box-button"></input>
                </form>
            </div>`;
            showcont.innerHTML = productsList
        }
        n += 3  
    }
}
window.onload(showProductsRows());

